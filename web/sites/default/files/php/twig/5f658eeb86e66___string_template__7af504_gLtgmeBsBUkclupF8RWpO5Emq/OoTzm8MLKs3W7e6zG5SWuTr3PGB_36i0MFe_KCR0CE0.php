<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__7af504657796b44275695e4f9b6276cd11ec6555136deb689d8781d6badb5a95 */
class __TwigTemplate_479836a7895742d2a2043f52616984fed1f4b67b933062fb70a8087b7b7c67e6 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 1];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"category\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_category"] ?? null)), "html", null, true);
        echo "</div>
<div class=\"research-category\">";
        // line 2
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_research_category"] ?? null)), "html", null, true);
        echo "</div>
<div class=\"title\">";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title"] ?? null)), "html", null, true);
        echo "</div>
<div class=\"share\">
<div class=\"social-share\">
    <div class=\"btn-item\">
        <a class=\"share-btn\">
            <img alt=\"share\" data-entity-type=\"file\" src=\"/sites/default/files/inline-images/share.svg\" />
        </a>
        <div class=\"social-wrapper\">
            <div class=\"socialbar\">
                <a class=\"close-share-btn\">
                    <img alt=\"share\" data-entity-type=\"file\" src=\"/sites/default/files/inline-images/share-focused.svg\" />
                </a>
                <a class=\"copy-btn\">
                    <img alt=\"share\" data-entity-type=\"file\" src=\"/sites/default/files/inline-images/icon-copy.svg\" />
                </a>
                <div class=\"copylink\">";
        // line 18
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["view_node_1"] ?? null)), "html", null, true);
        echo "</div>
                <a href=\"mailto:asd@asd.com\"><i class=\"far fa-envelope\">&nbsp;</i></a> 
                <a href=\"http://twitter.com/share?text=";
        // line 20
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_1"] ?? null)), "html", null, true);
        echo "&amp;url=https://tii.ae\" target=\"_blank\"><i class=\"fab fa-twitter\">&nbsp;</i></a> 
                <a href=\"https://www.linkedin.com/shareArticle?mini=true&amp;url=https://tii.ae&amp;title=";
        // line 21
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_1"] ?? null)), "html", null, true);
        echo "&amp;summary=&amp;source=LinkedIn\" target=\"_blank\"><i class=\"fab fa-linkedin\">&nbsp;</i></a>
            </div>
        </div>
    </div>
</div>
</div>";
    }

    public function getTemplateName()
    {
        return "__string_template__7af504657796b44275695e4f9b6276cd11ec6555136deb689d8781d6badb5a95";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 21,  87 => 20,  82 => 18,  64 => 3,  60 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__7af504657796b44275695e4f9b6276cd11ec6555136deb689d8781d6badb5a95", "");
    }
}
