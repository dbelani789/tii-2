/**
 * @file
 * Global utilities.
 *
 */
(function($, Drupal) {

  'use strict';

  Drupal.behaviors.tii = {
    attach: function(context, settings) {
      var position = $(window).scrollTop();
      $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
          $('body').addClass("scrolled");
        }
        else {
          $('body').removeClass("scrolled");
        }
        var scroll = $(window).scrollTop();
        if (scroll > position) {
          $('body').addClass("scrolldown");
          $('body').removeClass("scrollup");
        } else {
          $('body').addClass("scrollup");
          $('body').removeClass("scrolldown");
        }
        position = scroll;
      });
      // $('[dir="ltr"] .block-news .view-filters .form-text').attr('placeholder', 'Search news, upcoming events...');
      $('html[lang="ar"] .block-searchblock .content .form-item-keyword input').attr('placeholder', 'البحث عن الأخبار والأحداث القادمة ...');
      /*
      $('.block-research .view-content .views-row').each(function() {
        $(this).hover(function(){
          $(this).children('.views-field-field-icon').hide();
          $(this).children('.views-field-field-video-icon').show();
        }, function(){
          $(this).children('.views-field-field-icon').show();
          $(this).children('.views-field-field-video-icon').hide();
        });
      });*/
      $('.block-filternews [id^=edit-keyword], .block-filterevents [id^=edit-keyword], .block-medianews [id^=edit-keyword], .block-sticky-publications [id^=edit-keyword]').attr('placeholder', 'Search by keyword');
      $('html[lang="ar"] .block-filternews [id^=edit-keyword], html[lang="ar"] .block-filterevents [id^=edit-keyword], html[lang="ar"] .block-medianews [id^=edit-keyword], html[lang="ar"] .block-sticky-publications [id^=edit-keyword]').attr('placeholder', 'البحث عن طريق الكلمات الرئيسية');
      $('.page-view-research-centre #main-wrapper #main .region-content .view-research-centre .view-content .views-row').once().click(function() {
        $(this).siblings().children('.views-field-nothing').slideUp();
        $(this).children('.views-field-nothing').slideToggle();
        $(this).children('.views-field-nothing').toggleClass('active');
        if($(window).width() < 1600) {
          $(window).scrollTop(0);
        }
      });
      $('.page-view-subsite-research-centre #main-wrapper #main .region-content .view-subsite-research-centre .view-content .views-row').once().click(function() {
        $(this).siblings().find('.research-content-wrapper').slideUp();
        $(this).find('.research-content-wrapper').slideToggle();
        $(this).find('.research-content-wrapper').toggleClass('active');
      });
      $('a.contact-form-btn').once().click(function() {
        $('.block-contactus').slideToggle('right');
        $('.block-contactus').toggleClass('active');
      });
      $('.block-contactus a.close-btn').once().click(function() {
        $('.block-contactus').slideToggle('right');
        $('.block-contactus').toggleClass('active');
      });
      $(window).scroll(function (event) {
        var scroll = $(window).scrollTop();
        $('.scrollable-small').css('top', + scroll / 16);
      });
      $('.print-btn').once().click(function() {
        window.print();
        return false;
      });
      $('.scrollable-small .share-btn').once().click(function() {
        $('.scrollable-small .social-wrapper').fadeToggle();
        $('.scrollable-small .social-wrapper').toggleClass('active');
      });
      $('.social-share .share-btn').once().click(function() {
        $(this).next('.social-wrapper').fadeToggle();
        $(this).next('.social-wrapper').toggleClass('active');
      });
      $('.close-share-btn').once().click(function() {
        $(this).parents('.social-wrapper').fadeToggle();
        $(this).parents('.social-wrapper').toggleClass('active');
      });
      $('header nav.navigation .menu-icon, header .mobile-search, .block-searchblock .content .form-item-keyword input').once().click(function() {
        $('.block-search').fadeToggle();
        $('.block-search').toggleClass('active');
      });
      $('.search-close-btn').once().click(function() {
        $('.block-search').fadeToggle();
        $('.block-search').toggleClass('active');
      });
      $('.socialbar .copy-btn').once().click(function() {
        var $temp = $("<input>");
        $('body').append($temp);
        $temp.val($(this).next().text()).select();
        document.execCommand("copy");
        alert("Copied Link ");
        $temp.remove();
      });
      $('.leftcolumn .block-menu h2').once().click(function() {
        $(this).next().slideToggle();
      });
      $('.block .view-filters .form-row').after().once().click(function() {
        if($(window).width() < 991) {
          $(this).toggleClass('expanded');
        }
      });
      /*
      var nav = $('.team-section');
      if (nav.length) {
        if(window.location.hash) {
          var hash = window.location.hash;
          var $container = $('html,body');
          hash = hash.replace('#', '');
          $container.animate(
            { scrollTop: $('.' + hash).offset().top - $container.offset().top + $container.scrollTop(), scrollLeft: 0 }, 1500, 'swing');
        }
      }*/
    }
  };

  var referrer =  document.referrer;
  // console.log('previous url =>', referrer);
  if(referrer) {
    $('.team-back').attr('href', referrer + '#team-section');
  }

  function slideToggle(el, bShow){
    var $el = $(el), height = $el.data("originalHeight"), visible = $el.is(":visible");

    // if the bShow isn't present, get the current visibility and reverse it
    if( arguments.length == 1 ) bShow = !visible;

    // if the current visiblilty is the same as the requested state, cancel
    if( bShow == visible ) return false;

    // get the original height
    if( !height ){
      // get original height
      height = $el.show().height();
      // update the height
      $el.data("originalHeight", height);
      // if the element was hidden, hide it again
      if( !visible ) $el.css({height: '1px'}).hide();
    }

    // expand the knowledge (instead of slideDown/Up, use custom animation which applies fix)
    if( bShow ){
      $el.show().animate({height: height}, {duration: 500});
    } else {
      $el.animate({height: '1px'}, {duration: 500, complete:function (){
          $el.hide();
        }
      });
    }
  }

})(jQuery, Drupal);
