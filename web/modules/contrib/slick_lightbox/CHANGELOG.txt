
Slick Lightbox 8.x-1.0-dev, 2020-02-15
--------------------------------------
- Added the new libraries_directory_file_finder service.

Slick Lightbox 8.x-1.0-dev, 2020-01-24
--------------------------------------
- Fixed for initialSlide within Views gallery.

Slick Lightbox 8.x-1.0-dev, 2020-01-22
--------------------------------------
- Updated docs.
- Added possibility to fetch skin from optionset.
- Added Default skin by default to avoid broken display at first look.
- Changed url_attributes to use array for potential issues with empty Attribute.

Slick Lightbox 8.x-1.0-dev, 2020-01-19
--------------------------------------
- Fixed for broken slick with one slide, as usual. Unslick uncool.

Slick Lightbox 8.x-1.0-dev, 2019-03-08
--------------------------------------
- Added support for Blazy filter.

Slick Lightbox 8.x-1.0-dev, 2019-03-07
--------------------------------------
- Added CSS classes for Views UI.

Slick Lightbox 8.x-1.0-dev, 2019-02-27
--------------------------------------
- Added hook_slick_lightbox_attach_alter() for advanced usages.
- Added separate Slick settings and its lightbox for easy overrides as we have
  no UI yet.

Slick Lightbox 8.x-1.0-dev, 2019-02-12
--------------------------------------
- Added lazy option.
- Added basic skin when not using Slick formatter.

Slick Lightbox 8.x-1.0-dev, 2019-02-11
--------------------------------------
Initial commit.
