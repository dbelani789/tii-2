== Description ==
Domain access unique logo module allows the administrator to uplod unique logo 
while creating each domain.

If user want to use domain logo as customized way, 
variable available in hook_page_preprocess for custom usage.

variable name is $variables['domain_logo'].

== Notes ==
1. Domain access unique logo module have dependencies with domain access module

Installation
------------
1. Copy the entire  directory of daul to Drupal
/modules or /modules/contrib directory.

2. Login as an administrator to Enable the module in the "Administer" ->
   Modules (admin/modules)

3. Access the links to create custom domain.
   (admin/config/domain/add)

== Author ==
Arulraj
